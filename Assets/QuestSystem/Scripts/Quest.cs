using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//public struct QuestInfo
//{
//    public QuestInfo(string questID, string questName, string questDescription)
//    {
//        QuestID = questID;
//        QuestName = questName;
//        QuestDescription = questDescription;
//	}

//	public string QuestID { get; }
//	public string QuestName { get; }
//	public string QuestDescription { get; }
//}

[Serializable]
public class Quest
{
	[field: SerializeField]
	[field: Tooltip("Quest ID: For identifying quest in Quest Manager")]
	public string ID { get; private set; }

	[field: SerializeField]
	[field: Tooltip("Quest Name: Name of quest.\nFor display in UI")]
	public string Name { get; private set; }

	[field: SerializeField]
	[field: Tooltip("Quest Description: Description of quest.\nFor display in UI")]
	public string Description { get; private set; }

	[field: SerializeField]
	[field: Tooltip("List of objective(s) in this quest")]
	public List<Objective> Objectives { get; private set; } = new List<Objective>();

	public QuestNPC QuestNPC { get; private set; }

	public void InitQuestGiver(QuestNPC questNPC)
	{
		if(QuestNPC != null)
		{
			return;
		}
		QuestNPC = questNPC;
	}

    public void UpdateObjective(string objectiveID) 
    {
		int objectiveIndex = Objectives.FindIndex(0, Objectives.Count, obj => obj.ID.Equals(objectiveID));
		if (objectiveIndex < 0)
		{
			throw new Exception("Quest \"" + ID + "\" does not contain \"" + objectiveID + "\".");
		}
		Objective objective = Objectives[objectiveIndex];
		objective.UpdateObjective();
	}

	public bool IsQuestReadyToComplete()
	{
		bool result = true;
		foreach(Objective obj in Objectives) 
		{
			if(!obj.IsCompleted())
			{
				result = false;
				break;
			}
		}
		return result;
	}
}
