using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;
using UnityEngine.Events;

public enum QuestStatus
{
	NotStarted  = 0,
    Active      = 1,
    Completed   = 2,
}

public class QuestManager : MonoBehaviour
{
    public static QuestManager Instance;

	[field: SerializeField]
	[field: Tooltip("List of available quests and their info.")]
	public List<Quest> Quests { get; private set; } = new List<Quest>();
	[field: SerializeField]
	[field: Tooltip("List of id of active quests")]
	public List<string> ActiveQuests { get; private set; } = new List<string>();
	[field: SerializeField]
	[field: Tooltip("List of if of completed quests")]
	public List<string> CompletedQuests { get; private set; } = new List<string> ();

    void Awake()
    {
        if (Instance == null)
        {
			Instance = this;
			DontDestroyOnLoad(gameObject);
		}
        else
        {
            Destroy(this);
        }
    }

	public QuestStatus GetQuestStatus(string questID)
    {
		QuestStatus questStatus = QuestStatus.NotStarted;
        if(IsQuestActive(questID)) 
        {
            questStatus = QuestStatus.Active;
        }
        else if(IsQuestCompleted(questID)) 
        {
            questStatus = QuestStatus.Completed;
        }
        return questStatus;
	}

	public bool IsQuestActive(string questID)
	{
		int questIndex = ActiveQuests.FindIndex(0, ActiveQuests.Count, QuestID => QuestID.Equals(questID));
		return questIndex != -1;
	}

	public bool IsQuestCompleted(string questID)
	{
		int questIndex = CompletedQuests.FindIndex(0, CompletedQuests.Count, QuestID => QuestID.Equals(questID));
		return questIndex != -1;
	}

    public void CompleteQuest(string questID)
	{
        // ensure quest is active
		if (!IsQuestActive(questID))
		{
			throw new System.Exception("Unable to complete an inactive quest, " + questID + "!");
		}

        // ensure quest exists
		Quest quest = GetQuest(questID);
        if(quest == null) 
        {
            throw new System.Exception("Unable to complete an non-existent quest, " + questID + "!");
        }

        ActiveQuests.Remove(questID);
        CompletedQuests.Add(questID);
	}

    public void AcceptQuest(string questID)
    {
        if(ActiveQuests.Contains(questID))
        {
            return;
        }

        if(CompletedQuests.Contains(questID))
        {
            return;
        }

        ActiveQuests.Add(questID);
	}

	public Quest GetQuest(string questID)
    {
        int questIndex = Quests.FindIndex(0, Quests.Count, Quest => Quest.ID.Equals(questID));
        Quest toReturn = null;
        if(questIndex >= 0)
        { 
            toReturn = Quests[questIndex];
        }
        return toReturn;
	}

    public void UpdateObjective(string questID, string objectiveID)
	{
        Quest questToUpdate = GetQuest(questID);
		if (questToUpdate == null)
		{
			Debug.Log("Quest \"" + questID + "\" does not exist but trying to complete \"" + objectiveID + "\".");
			return;
		}
		if (!IsQuestActive(questID))
		{
			Debug.Log("Quest \"" + questID + "\" is inactive but trying to complete \"" + objectiveID + "\".");
		}

		questToUpdate.UpdateObjective(objectiveID);
	}
}
