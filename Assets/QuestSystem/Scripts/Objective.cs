using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

//public readonly struct ObjectiveInfo
//{
//	public ObjectiveInfo(string objectiveID, string objectiveName, string objectiveDescription)
//	{
//		ObjectiveID = objectiveID;
//		ObjectiveName = objectiveName;
//		ObjectiveDescription = objectiveDescription;
//	}

//	public string ObjectiveID { get; }
//	public string ObjectiveName { get; }
//	public string ObjectiveDescription { get; }
//}

[Serializable]
public class Objective : IEquatable<Objective>
{
	[field: SerializeField]
	[field: Tooltip("Objective ID: For identifying quest in Quest Manager and in Quest")]
	public string ID { get; private set; }
	[field: SerializeField]
	[field: Tooltip("Objective Name: Name / Short description of objective.\nFor display in UI")]
	public string Name { get; private set; }
	[field: SerializeField]
	[field: Tooltip("Objective Description: Long description of objective.\nFor display in UI.")]
	public string Description { get; private set; }
	[field: SerializeField]
	[field: Tooltip("Maximum number of times the objective has to be updated before completion.")]
	public int MaxCount { get; private set; } = 1;
	int CurrentCount;

	public bool Equals(Objective other)
	{
		if (other == null) return false;
		return (ID.Equals(other.ID));
	}

	// might not be needed
	public void UpdateObjective()
	{
		UpdateCount(1);
	}

	public void UpdateCount(int count)
	{
		CurrentCount += count;
		if(CurrentCount >= MaxCount)
		{
			Debug.Log("Objective completed!");
		}
	}

	public bool IsCompleted()
	{
		return CurrentCount >= MaxCount;
	}

	public string GetMaxCountFormattedDescription()
	{
		string result = Description.Replace("MaxCount", "0");
		return string.Format(result, MaxCount);
	}

	public string GetProgressFormattedDescription()
	{
		string result = Description.Replace("{MaxCount}", "{0}/{1}");
		return string.Format(result, CurrentCount, MaxCount);
	}
}
