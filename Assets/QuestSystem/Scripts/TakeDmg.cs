using Etra.StarterAssets.Interactables.Enemies;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeDmg : MonoBehaviour, IDamageable<int>
{
	public void TakeDamage(int damage)
    {
        Debug.Log("Damage taken = " + damage);
    }
}
