using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ObjectiveTracker : MonoBehaviour
{
    [SerializeField]
    protected string QuestID;
    [SerializeField]
	protected string ObjectiveID;

    public UnityEvent<GameObject> OnObjectiveDestroyed;

	protected bool ApplicationQuitStarted = false;

	public void InitTracker(string questID, string objectiveID)
    {
        QuestID = questID; 
        ObjectiveID = objectiveID;
    }

	protected virtual void OnDestroy()
	{
		// do not update QuestManager if application was terminated.
		if (ApplicationQuitStarted)
		{
			return;
		}
        OnObjectiveDestroyed.Invoke(gameObject);
	}


	void OnApplicationQuit()
	{
		ApplicationQuitStarted = true;
	}
}
