using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestNPC : MonoBehaviour
{
	[field: SerializeField]
	[field: Tooltip("List of quest id that can be started here.")]
	public List<string> AvailableQuests { get; protected set; } = new List<string>();
	[field: SerializeField]
	[field: Tooltip("List of quest id that can be ended here.")]
	public List<string> CompletableQuests { get; protected set; } = new List<string>();
}
