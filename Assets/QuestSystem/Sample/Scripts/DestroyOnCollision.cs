using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnCollision : MonoBehaviour
{
    [SerializeField]
    List<string> DestroyTags = new List<string>();

	private void OnCollisionEnter(Collision collision)
	{
		if (!DestroyTags.Contains(collision.gameObject.tag))
        {
            return; 
        }

        Destroy(gameObject);
	}
}
