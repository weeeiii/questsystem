using Etra.StarterAssets.Abilities;
using Etra.StarterAssets.Items;
using Etra.StarterAssets;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static Etra.StarterAssets.Items.EtraFPSUsableItemManager;
using Unity.VisualScripting;
using UnityEngine.Events;

public class UI_QuestCanvas : MonoBehaviour
{
	public static UI_QuestCanvas Instance;

	//UIStateMachine UIStateMachine	= null;
	QuestNPC CurrentQuestNPC = null;

	[field: SerializeField]
	[field: Tooltip("UI Panel showing available quests and quest info ")]
	public GameObject UI_QuestPanel { get; private set; }
	[field: SerializeField]
	[field: Tooltip("Button to close UI_QuestPanel.")]
	public GameObject UI_QuestPanelCloseButton { get; private set; }
	[field: SerializeField]
	[field: Tooltip("UI Panel showing list of available quests.")]
	public GameObject UI_QuestListPanel { get; private set; }
	[field: SerializeField]
	[field: Tooltip("Button to return from quest info to available quests.")]
	public GameObject UI_QuestInfoBackButton { get; private set; }
	[field: SerializeField]
	[field: Tooltip("UI Panel to add available quests.")]
	public GameObject UI_AvailableQuestsPanel { get; private set; }
	[field: SerializeField]
	[field: Tooltip("UI Panel showing quest info.")]
	public GameObject UI_QuestInfoPanel { get; private set; }
	[field: SerializeField]
	[field: Tooltip("UI Panel showing quest info.")]
	public GameObject UI_QuestButtonPrefab { get; private set; }
	[field: SerializeField]
	[field: Tooltip("UI Panel showing active quests.")]
	public GameObject UI_ActiveQuestPanel { get; private set; }

	void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
		}
		else
		{
			Destroy(this);
		}
	}

	public void ShowAvailableQuests()
	{
		if (CurrentQuestNPC == null)
		{
			throw new System.Exception("QuestUI.ShowAvailableQuests(): No NPC to get quests to show!");
		}

		UI_QuestListPanel.SetActive(true);
		UI_QuestInfoPanel.SetActive(false);
		ShowCloseButton();
		UI_QuestInfoBackButton.SetActive(false);

		// clear all existing children of available quest panel
		int existingChildrenCount = UI_AvailableQuestsPanel.transform.childCount;
		for (int i = 0; i < existingChildrenCount; ++i)
		{
			GameObject child = UI_AvailableQuestsPanel.transform.GetChild(i).gameObject;
			Destroy(child);
		}
		UI_AvailableQuestsPanel.transform.DetachChildren();

		// get all quests from quest giver
		List<string> availableQuests = CurrentQuestNPC.AvailableQuests;
		List<string> completableQuests = CurrentQuestNPC.CompletableQuests;
		// display quests available and completable at current quest npc
		for (int i = 0; i < availableQuests.Count; ++i)
		{
			string questID = availableQuests[i];
			// Do not display quests that are completed
			if(QuestManager.Instance.GetQuestStatus(questID) == QuestStatus.Completed)
			{
				continue;
			}
			// Do not display quests that are not completable here
			if(QuestManager.Instance.GetQuestStatus(questID) == QuestStatus.Active && !completableQuests.Contains(questID))
			{
				continue;
			}
			AddQuestToPanel(questID);
		}
		// display quests available and completable at current quest npc
		for (int i = 0; i < completableQuests.Count; ++i)
		{
			string questID = completableQuests[i];
			// Do not display quests that are completed
			if (QuestManager.Instance.GetQuestStatus(questID) == QuestStatus.Completed)
			{
				continue;
			}
			// Do not display quests that are completable but inactive
			if (QuestManager.Instance.GetQuestStatus(questID) == QuestStatus.NotStarted)
			{
				continue;
			}
			// Do not add duplicate quest entries
			if(availableQuests.Contains(questID))
			{
				continue;
			}
			AddQuestToPanel(questID);
		}
	}

	public void ShowAvailableQuests(QuestNPC questNPC)
	{
		CurrentQuestNPC = questNPC;
		ShowAvailableQuests();
	}

	void AddQuestToPanel(string questID)
	{
		Quest quest = QuestManager.Instance.GetQuest(questID);
		quest.InitQuestGiver(CurrentQuestNPC);

		// create an instance of quest button prefab
		GameObject questButtonInst = Instantiate(UI_QuestButtonPrefab, UI_AvailableQuestsPanel.transform, false);
		// set name of quest as text on quest button
		UI_QuestButton uiQuestButton = questButtonInst.GetComponent<UI_QuestButton>();
		uiQuestButton.InitButton(quest);
	}

	public void ShowQuestInfo(Quest quest)
	{
		// show info of selected quest
		UI_QuestListPanel.SetActive(false);
		UI_QuestInfoPanel.SetActive(true);
		UI_QuestInfoBackButton.SetActive(true);

		UI_QuestInfo uiQuestInfo = UI_QuestInfoPanel.GetComponent<UI_QuestInfo>();
		uiQuestInfo.SetupQuestPanel(quest);
	}

	public void CloseQuestPanel()
	{
		EnableEtraCharacterControls(true);
		// deactiate all children
		if (UI_QuestPanel == null)
		{
			return;
		}

		for (int i = 0; i < UI_QuestPanel.transform.childCount; ++i)
		{
			GameObject child = UI_QuestPanel.transform.GetChild(i).gameObject;
			child.SetActive(false);
		}
	}

	void ShowCloseButton()
	{
		if (UI_QuestPanelCloseButton == null)
		{
			return;
		}

		UI_QuestPanelCloseButton.SetActive(true);
		EnableEtraCharacterControls(false);
	}

	void EnableEtraCharacterControls(bool isEnable)
	{
		// Show/Hide cursor
		Cursor.lockState = isEnable ? CursorLockMode.Locked : CursorLockMode.None;

		EtraCharacterMainController character = FindObjectOfType<EtraCharacterMainController>();

		// Enable/Disable character and camera movement
		EtraAbilityManager abilityManager = character.etraAbilityManager;
		ABILITY_CharacterMovement moveAbility = abilityManager.GetComponent<ABILITY_CharacterMovement>();
		moveAbility.abilityEnabled = isEnable;
		ABILITY_CameraMovement camAbility = abilityManager.GetComponent<ABILITY_CameraMovement>();
		camAbility.abilityEnabled = isEnable;

		// Enable/Disable usage of items
		EtraFPSUsableItemManager usableItemManager = character.etraFPSUsableItemManager;
		if (isEnable)
		{
			usableItemManager.enableFPSItemInputs();
		}
		else
		{
			usableItemManager.disableFPSItemInputs();
		}
	}

	public void UpdateActiveQuestList(string questID)
	{
		UI_ActiveQuestPanel.SetActive(QuestManager.Instance.ActiveQuests.Count > 0);
		UI_ActiveQuestList activeQuestList = UI_ActiveQuestPanel.GetComponent<UI_ActiveQuestList>();
		activeQuestList.UpdateQuest(questID);
	}
}
