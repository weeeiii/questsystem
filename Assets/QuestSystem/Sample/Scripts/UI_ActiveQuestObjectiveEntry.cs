using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UI_ActiveQuestObjectiveEntry : MonoBehaviour
{
	[field: SerializeField]
    [field: Tooltip("Objective shown in this entry.")]
    public Objective Objective { get; private set; }
	[field: SerializeField]
	[field: Tooltip("Objective status image.")]
	public Image UI_ObjectiveStatusImage { get; private set; }
	[field: SerializeField]
	[field: Tooltip("Objective formatted description.")]
	public TextMeshProUGUI UI_ObjectiveText { get; private set; }

    public void InitEntry(Objective objective)
    {
		Objective = objective;
		SetDisplayTest();
		SetButtonStatus();

		// set position in parent
		RectTransform parentRect = transform.parent.GetComponent<RectTransform>();
		transform.localPosition = new Vector3(0.0f, -parentRect.rect.height, 0.0f);

		// update parent panel size
		RectTransform selfRect = GetComponent<RectTransform>();		
		parentRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, parentRect.rect.height + UI_ObjectiveText.preferredHeight);
	}

	void SetDisplayTest()
	{
		UI_ObjectiveText.text = Objective.GetProgressFormattedDescription();
	}

	void SetButtonStatus()
	{
		if(Objective.IsCompleted())
		{
			UI_ObjectiveStatusImage.color = Color.green;
		}
		else
		{
			UI_ObjectiveStatusImage.color = Color.yellow;
		}
	}

	public void UpdateEntry()
	{
		SetDisplayTest();
		SetButtonStatus();
	}
}
