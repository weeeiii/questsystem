using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VolumeObjectiveSpawner : ObjectiveSpawner
{
	[field: SerializeField]
	[field: Tooltip("Box/Sphere collider to spawn objectives in\nWill spawn at world position if left empty")]
	public Collider SpawningVolume { get; private set; } = null;
	// Random x,y,z and spawn

	protected override void SpawnObjective()
	{
		if (SpawningVolume == null)
		{
			base.SpawnObjective();
			return;
		}

		// get spawn point
		Vector3 spawnLocation = GetRandomPointInCollider(SpawningVolume);
		GameObject ObjectiveInst = Instantiate(ObjectivePrefab, spawnLocation, Quaternion.identity);
		PostSpawnObjective(ObjectiveInst);
	}
}
