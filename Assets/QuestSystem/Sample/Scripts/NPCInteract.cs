using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class NPCInteract : MonoBehaviour
{
    [SerializeField]
    List<string> InteractableTags = new List<string>();
	[SerializeField]
	UnityEvent OnInteractStart;

	private void OnTriggerEnter(Collider other)
	{
        if(!InteractableTags.Contains(other.tag))
        {
            return;
        }
        OnInteractStart.Invoke();
	}
}
