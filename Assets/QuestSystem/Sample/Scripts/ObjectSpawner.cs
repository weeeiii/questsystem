using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour
{
    public float SpawnDelay = 1.0f;
    public GameObject ObjectPrefab = null;
    float SpawnCooldown = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        SpawnCooldown -= Time.deltaTime;
        if(SpawnCooldown <= 0.0f)
        {
			GameObject ObjectiveInst = Instantiate(ObjectPrefab, transform.position, transform.rotation);
            SpawnCooldown = SpawnDelay;
		}		
	}


}
