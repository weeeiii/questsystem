using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class UpdateObjectiveOnCollision : ObjectiveTracker
{
	[SerializeField]
	List<string> CollisionTags = new List<string>();

	private void OnCollisionEnter(Collision collision)
	{
		if (!CollisionTags.Contains(collision.gameObject.tag))
		{
			return;
		}

		// update quest/quest manager
		QuestManager.Instance.UpdateObjective(QuestID, ObjectiveID);
		UI_QuestCanvas.Instance.UpdateActiveQuestList(QuestID);
	}
}
