using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UI_QuestInfo : MonoBehaviour
{
	[HideInInspector]
	public Quest Quest { get; private set; }

	public TextMeshProUGUI UI_QuestNameText;
	public TextMeshProUGUI UI_QuestDescriptionText;
	public GameObject UI_ObjectiveScrollview;
	public GameObject UI_ObjectivePrefab;
	public GameObject UI_AcceptButton;
	public GameObject UI_CompleteButton;

	public void SetupQuestPanel(Quest quest)
    {
		// clear all existing objectives
		for (int i = 0; i < UI_ObjectiveScrollview.transform.childCount;)
		{
			Transform child = UI_ObjectiveScrollview.transform.GetChild(i);
			child.SetParent(null);
			Destroy(child.gameObject);
		}

        Quest = quest;
		UI_QuestNameText.text = Quest.Name;
		UI_QuestDescriptionText.text = Quest.Description;
        for (int i = 0; i < Quest.Objectives.Count; ++i)
        {
            Objective objective = Quest.Objectives[i];
            GameObject objectiveInst = Instantiate(UI_ObjectivePrefab, UI_ObjectiveScrollview.transform, false);
			objectiveInst.GetComponent<UI_Objective>().InitObjective(objective);
		}

		// check if it is activated by quest starter or quest ender
		QuestStatus questStatus = QuestManager.Instance.GetQuestStatus(Quest.ID);
		switch (questStatus)
		{
			case QuestStatus.NotStarted:
				UI_AcceptButton.SetActive(true);
				UI_CompleteButton.SetActive(false);
				break;
			case QuestStatus.Active:
				UI_AcceptButton.SetActive(false);
				UI_CompleteButton.SetActive(true);
				UI_CompleteButton.GetComponent<Button>().interactable = Quest.IsQuestReadyToComplete();
				break;
			case QuestStatus.Completed:
				UI_AcceptButton.SetActive(false);
				UI_CompleteButton.SetActive(false);
				break;
		}
	}

    public void AcceptQuest()
    {
        QuestManager.Instance.AcceptQuest(Quest.ID);
		UI_QuestCanvas.Instance.ShowAvailableQuests(Quest.QuestNPC);
		UI_QuestCanvas.Instance.UpdateActiveQuestList(Quest.ID);
	}

	public void CompleteQuest()
	{
		QuestManager.Instance.CompleteQuest(Quest.ID);
		UI_QuestCanvas.Instance.ShowAvailableQuests();
		UI_QuestCanvas.Instance.UpdateActiveQuestList(Quest.ID);
	}
}
