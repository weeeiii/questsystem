using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UI_ActiveQuestEntry : MonoBehaviour
{
	[field: SerializeField]
	[field: Tooltip("Quest of this panel.")]
	public Quest Quest { get; private set; }

	[field: SerializeField]
	[field: Tooltip("UI showing name of quest")]
	public TextMeshProUGUI UI_ActiveQuestName { get; private set; }

	[field: SerializeField]
	[field: Tooltip("Objective entry prefab to spawn.")]
	public GameObject UI_ActiveQuestObjectiveEntryPrefab { get; private set; }

	List<UI_ActiveQuestObjectiveEntry> ActiveQuestObjectiveEntries = new List<UI_ActiveQuestObjectiveEntry>();
    public float UI_Height { get; private set; }

    public void InitQuestEntry(Quest quest)
    {
        Quest = quest;
		UI_ActiveQuestName.text = Quest.Name;
		float panelHeight = transform.GetComponent<RectTransform>().rect.height;
		float textHeight = UI_ActiveQuestName.preferredHeight;
		// Resize panel size if text overflowed
		if (panelHeight < textHeight)
		{
			RectTransform selfRect = GetComponent<RectTransform>();
			selfRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, textHeight);
		}

		// set position in parent
		int childCount = transform.parent.childCount;
        float offsetY = 0.0f;
        for(int i = 1; i < childCount; ++i)
        {
			Transform child = transform.parent.GetChild(i - 1);
            RectTransform childRect = child.GetComponent<RectTransform>();
            offsetY += childRect.rect.height;
		}
		transform.localPosition -= new Vector3(0.0f, offsetY, 0.0f);

        // add objectives
        for(int i = 0; i < Quest.Objectives.Count; ++i)
        {
            GameObject activeObjectiveEntryInst = Instantiate(UI_ActiveQuestObjectiveEntryPrefab, transform, false);
            UI_ActiveQuestObjectiveEntry activeObjectiveEntry = activeObjectiveEntryInst.GetComponent<UI_ActiveQuestObjectiveEntry>();
            activeObjectiveEntry.InitEntry(Quest.Objectives[i]);
            ActiveQuestObjectiveEntries.Add(activeObjectiveEntry);
		}

		// update UI_Height
		RectTransform rect = GetComponent<RectTransform>();
        UI_Height = rect.rect.height;
	}

    public void RepositionEntry(float offsetY)
	{
		transform.localPosition += new Vector3(0.0f, offsetY, 0.0f);
	}

    public void UpdateEntry()
    {
        foreach(UI_ActiveQuestObjectiveEntry entry in ActiveQuestObjectiveEntries) 
        {
            entry.UpdateEntry();
        }
    }
}
