using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundAtPositionOnDestroy : MonoBehaviour
{
	[SerializeField]
	AudioClip AudioClip = null;

	void OnDestroy()
	{
		if (!AudioClip)
		{
			return;
		}

		AudioSource.PlayClipAtPoint(AudioClip, transform.position);
	}
}
