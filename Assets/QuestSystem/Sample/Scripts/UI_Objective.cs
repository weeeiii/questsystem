using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UI_Objective : MonoBehaviour
{
    public Objective Objective { get; private set; }
	public GameObject UI_CompletedIcon;
	public GameObject UI_NotCompletedIcon;
	public TextMeshProUGUI UI_ObjectiveText;

	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void InitObjective(Objective objective)
	{
		// position objective ui
		RectTransform objRect = GetComponent<RectTransform>();
		float offsetY = objRect.rect.height * (transform.parent.childCount - 1);
		transform.localPosition -= new Vector3(0.0f, offsetY, 0.0f);
		// set objective
		Objective = objective;
		UI_ObjectiveText.text = objective.GetMaxCountFormattedDescription();
		UI_CompletedIcon.SetActive(Objective.IsCompleted());
		UI_NotCompletedIcon.SetActive(!Objective.IsCompleted());
    }
}
