using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateObjectiveOnTrigger : ObjectiveTracker
{
	[SerializeField]
	List<string> TriggerTags = new List<string>();

	private void OnTriggerEnter(Collider other)
	{
		if (!TriggerTags.Contains(other.tag))
		{
			return;
		}

		// update quest/quest manager
		QuestManager.Instance.UpdateObjective(QuestID, ObjectiveID);
		UI_QuestCanvas.Instance.UpdateActiveQuestList(QuestID);
	}
}
