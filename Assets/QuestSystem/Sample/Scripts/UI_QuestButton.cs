using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UI_QuestButton : MonoBehaviour
{
	[HideInInspector]
	public Quest Quest;
	[field: SerializeField]
	[field: Tooltip("Quest button text: Set text based on quest")]
	public TextMeshProUGUI UI_ButtonText { get; private set; }

	[field: SerializeField]
	[field: Tooltip("Quest status image")]
	public Image UI_QuestStatusImage { get; private set; }

    public void InitButton(Quest quest)
	{
        //init variable values
        Quest = quest;
		SetButtonText();
		SetButtonPosition();
		SetButtonStatus();
	}

    void SetButtonPosition()
	{
		float panelHeight = transform.GetComponent<RectTransform>().rect.height;
		float textHeight = UI_ButtonText.preferredHeight;
		// Resize panel size if text overflowed
		if(panelHeight < textHeight)
		{
			RectTransform selfRect = GetComponent<RectTransform>();
			selfRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, textHeight);
		}

		int childCount = transform.parent.childCount;
		float offsetY = 0.0f;
		for (int i = 0; i < childCount - 1; i++) 
		{
			offsetY += transform.parent.GetChild(i).GetComponent<RectTransform>().rect.height;
		}
		transform.localPosition -= new Vector3(0.0f, offsetY, 0.0f);
    }
    void SetButtonText()
    {
        if(UI_ButtonText == null)
        {
            return;
        }

        if(Quest== null)
		{
			return;
		}

		UI_ButtonText.text = Quest.Name;
    }

    void SetButtonStatus()
    {
        QuestStatus status = QuestManager.Instance.GetQuestStatus(Quest.ID);
        switch(status)
		{
			case QuestStatus.NotStarted:
				UI_QuestStatusImage.color = Color.red;
				break;

			case QuestStatus.Active:
				UI_QuestStatusImage.color = Quest.IsQuestReadyToComplete() ? Color.green : Color.yellow;				
				break;

			case QuestStatus.Completed:
				UI_QuestStatusImage.color = Color.green;
				break;
		}
	}

	public void Activate()
	{
		UI_QuestCanvas.Instance.ShowQuestInfo(Quest);
	}
}
