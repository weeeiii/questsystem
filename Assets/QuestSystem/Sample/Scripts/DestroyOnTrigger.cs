using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnTrigger : MonoBehaviour
{
    [SerializeField]
	List<string> DestroyTags = new List<string>();

    void OnTriggerEnter(Collider other)
	{
		if (!DestroyTags.Contains(other.tag))
        { 
            return; 
        }
        Destroy(gameObject);
    }
}
