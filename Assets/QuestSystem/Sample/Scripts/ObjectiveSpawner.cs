using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ObjectiveSpawner : MonoBehaviour
{
	[SerializeField]
	[Tooltip("Prefab to spawn")]
	protected GameObject ObjectivePrefab = null;

	[SerializeField]
	[Tooltip("Number of Objectives spawnable in a second")]
	protected float SpawnRate = 0.0f;
	protected float SpawnCooldown = 0.0f;
	protected float SpawnTimeToWait = 0.0f;

	[SerializeField]
	[Tooltip("Maximum number of objectives spawnable")]
	protected int MaxSpawnCount = 0;
	protected List<GameObject> ObjectiveInstances = new List<GameObject>();

	[SerializeField]
	[Tooltip("Set to true/false to enable/disable spawning")]
	protected bool IsActive = false;
	[SerializeField]
	[Tooltip("Set to true/false to enable/disable spawning when quest is active")]
	protected bool IsSpawningWhenQuestIsActive = false;

	[SerializeField]
	[Tooltip("QuestID to set in ObjectiveTracker")]
	protected string QuestID = "";
	[SerializeField]
	[Tooltip("ObjectiveID to set in ObjectiveTracker")]
	protected string ObjectiveID = "";

	void Start()
	{
		SpawnTimeToWait = 1 / SpawnRate;
	}

	// Update is called once per frame
	void Update()
    {
		if (!IsActive)
		{
			return;
		}
		if(IsSpawningWhenQuestIsActive && !QuestManager.Instance.IsQuestActive(QuestID))
		{
			return;
		}

        if(CanSpawn() && ObjectivePrefab != null) 
		{
			SpawnObjective();
		}
		else
		{
			SpawnCooldown -= Time.deltaTime;
		}
    }

	bool CanSpawn()
	{
		if(ObjectiveInstances.Count >= MaxSpawnCount)
		{
			return false;
		}

		if(SpawnCooldown > 0)
		{
			return false;
		}
		return true;
	}

	protected virtual void SpawnObjective()
	{
		GameObject ObjectiveInst = Instantiate(ObjectivePrefab);
		PostSpawnObjective(ObjectiveInst);
	}

	protected void PostSpawnObjective(GameObject objectiveInst)
	{
		AttachTracker(objectiveInst);
		ObjectiveInstances.Add(objectiveInst);
		SpawnCooldown = SpawnTimeToWait;
	}

	protected void AttachTracker(GameObject objectiveInstance)
	{
		ObjectiveTracker tracker = objectiveInstance.GetComponent<ObjectiveTracker>();
		if (tracker == null)
		{
			tracker = objectiveInstance.AddComponent<ObjectiveTracker>();
		}
		tracker.InitTracker(QuestID, ObjectiveID);
		tracker.OnObjectiveDestroyed.AddListener((GameObject destroyedObj) =>
		{
			ObjectiveInstances.Remove(destroyedObj);
		});
	}

	protected Vector3 GetRandomPointInCollider(Collider collider)
	{
		float randX = Random.Range(collider.bounds.min.x, collider.bounds.max.x);
		float randY = Random.Range(collider.bounds.min.y, collider.bounds.max.y);
		float randZ = Random.Range(collider.bounds.min.z, collider.bounds.max.z);

		return new Vector3(randX, randY, randZ);
	}
}
