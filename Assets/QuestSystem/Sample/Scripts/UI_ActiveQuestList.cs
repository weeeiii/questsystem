using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_ActiveQuestList : MonoBehaviour
{
	[field: SerializeField]
	[field: Tooltip("UI Panel showing active quests and respective formatted objectives.")]
	public GameObject UI_ActiveQuestListPanel { get; private set; }

	[field: SerializeField]
	[field: Tooltip("UI Panel of a quest entry, prefab.")]
	public GameObject UI_ActiveQuestEntryPrefab { get; private set; }

	List<UI_ActiveQuestEntry> ActiveQuestEntries = new List<UI_ActiveQuestEntry>();

	public void UpdateQuest(string questID)
	{
		UI_ActiveQuestEntry entry = GetQuestEntry(questID);
		if (entry == null) 
		{
			if(!QuestManager.Instance.IsQuestActive(questID))
			{ 
				return; 
			}
			AddQuest(QuestManager.Instance.GetQuest(questID));
		}
		else
		{
			if (QuestManager.Instance.IsQuestCompleted(questID))
			{
				int entryIndex = GetQuestEntryIndex(questID);
				RemoveQuest(entry, entryIndex);
			}
			else
			{
				entry.UpdateEntry();
			}
		}
	}

	void AddQuest(Quest quest)
	{
		GameObject questEntryInst = Instantiate(UI_ActiveQuestEntryPrefab, UI_ActiveQuestListPanel.transform, false);
		UI_ActiveQuestEntry entry = questEntryInst.GetComponent<UI_ActiveQuestEntry>();
		entry.InitQuestEntry(quest);
		ActiveQuestEntries.Add(entry);
	}

	void RemoveQuest(UI_ActiveQuestEntry entry, int entryIndex)
	{
		ActiveQuestEntries.Remove(entry);
		entry.gameObject.transform.SetParent(null);
		float offsetY = entry.UI_Height;
		Destroy(entry.gameObject);

		for (int i = entryIndex; i < UI_ActiveQuestListPanel.transform.childCount; i++) 
		{
			Transform child = UI_ActiveQuestListPanel.transform.GetChild(i);
			UI_ActiveQuestEntry childQuestEntry = child.GetComponent<UI_ActiveQuestEntry>();
			childQuestEntry.RepositionEntry(offsetY);
		}
	}

	UI_ActiveQuestEntry GetQuestEntry(string questID)
	{
		UI_ActiveQuestEntry entry = ActiveQuestEntries.Find(entry => entry.Quest.ID.Equals(questID));
		return entry;
	}

	int GetQuestEntryIndex(string questID)
	{
		int index = ActiveQuestEntries.FindIndex(entry => entry.Quest.ID.Equals(questID));
		return index;
	}
}
